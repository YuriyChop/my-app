import './App.css';
import React from 'react';

function App() {
  const planets = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune', 'Pluto'];
  const changeTheme = () => {
    document.body.classList.toggle('dark');
    document.getElementById('planets-list').classList.toggle('dark');
  }
  return (
    <div className='App'>
      {React.createElement('h1', { style: { color: '#999', fontSize: '19px' } }, 'Solar system planets')}
      <ul className='planets-list' id='planets-list'>
        {planets.map((planet) => (
          <li key={planet}>{planet}</li>
        ))}
      </ul>
      
      <label className='switch' htmlFor='checkbox'>
        <input type='checkbox' id='checkbox' />
        <div className='slider round' onClick={changeTheme}></div>
      </label>
    </div>
  );
}

export default App;